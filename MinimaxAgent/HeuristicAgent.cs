﻿using CommonAgentFunc;
using Enumerations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinimaxAgent
{
    class HeuristicAgent
    {

        private ProcessStartInfo _agent_exe_info;
        private bool _can_access_exe;
        private Process _h_agent_process;

        public bool CanAccessExe
        {
            get
            {
                return _can_access_exe;
            }
        }

        public HeuristicAgent(string exePath)
        {
            _can_access_exe = false;
            if (File.Exists(exePath))
            {
                _agent_exe_info = new ProcessStartInfo(exePath);
                _agent_exe_info.CreateNoWindow = true;
                _agent_exe_info.UseShellExecute = false;
                _agent_exe_info.Arguments = String.Format("{0}{1}{2}",Board.NEUTRAL_CHAR , Board.BLACK_CHAR , Board.WHITE_CHAR);
                _can_access_exe = true;
            }
        }

        public bool StartHeuristicAgent()
        {
            if (_can_access_exe)
            {
                try
                {
                    _h_agent_process = Process.Start(_agent_exe_info);
                    return true;
                }catch(Exception ex)
                {
                    
                }
            }
            return false;
        }

        public void Kill()
        {
            _h_agent_process.Kill();
        }

        public int GetHeuristicValue(Board currentBoard, Color maxColor, Color lastColorPlayed)
        {
            if (_can_access_exe)
            {
                string destination = "HeuristicAgent";
                StringBuilder data = new StringBuilder(currentBoard.ParameterString);
                data.Append(maxColor.ToChar());
                data.Append(lastColorPlayed.ToChar());
                PipeHelper.PushData(destination, data.ToString());
                string res = PipeHelper.FetchData(destination);
                if(res == null)
                {
                    return int.MinValue;
                }else
                {
                    return int.Parse(res);
                }
                
                
            }
            return int.MinValue;

        }
    }
}
