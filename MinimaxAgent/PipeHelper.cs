﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CommonAgentFunc
{
    public static class PipeHelper
    {
        private static int MAX_RETRIES = 5;
        public static bool PushData(string to, string data)
        {
            try
            {
                using (var pipeServer = new NamedPipeServerStream(to, PipeDirection.Out))
                {
                    pipeServer.WaitForConnection();
                    if (pipeServer.IsConnected)
                    {
                        using (var sw = new StreamWriter(pipeServer))
                        {
                            sw.WriteLine(data);
                            
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static string FetchData(string from)
        {
            int nbRetries = 0;
            while (nbRetries < MAX_RETRIES)
            {
                try
                {

                    using (var pipeClient = new NamedPipeClientStream(".", from, PipeDirection.In))
                    {
                        pipeClient.Connect(1000);
                        if (pipeClient.IsConnected)
                        {
                            using (var sr = new StreamReader(pipeClient))
                            {
                                string res = sr.ReadLine();
                                return res;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }

                }
                catch (TimeoutException ex)
                {
                    ++nbRetries;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;

        }

    }
}
