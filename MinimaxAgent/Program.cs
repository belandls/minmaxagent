﻿using CommonAgentFunc;
using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinimaxAgent
{
    class Program
    {
        static string BOARD_FILENAME = "board.txt";
        static string INI_FILENAME = "conf.ini";
        static int DEFAULT_N_PLY = 5;

        static int Main(string[] args)
        {
            //Debugger.Launch();
            Board.NEUTRAL_CHAR = args[2][0];
            Board.BLACK_CHAR = args[2][1];
            Board.WHITE_CHAR = args[2][2];

            Board b = GetBoardFromFile();


            int nPly;
            var parser = new FileIniDataParser();
            IniData ini;
            if (File.Exists(INI_FILENAME))
            {
                ini = parser.ReadFile(INI_FILENAME);
                nPly = int.Parse(ini["Configurations"]["N_PLY"]);
            }
            else
            {
                ini = new IniData();
                nPly = DEFAULT_N_PLY;
                ini.Sections.AddSection("Configurations");
                ini["Configurations"].AddKey("N_PLY", DEFAULT_N_PLY.ToString());
                parser.WriteFile(INI_FILENAME, ini);
            }

            Minimax mm;
            HeuristicAgent ha = new HeuristicAgent(args[0]);
            if (ha.CanAccessExe && ha.StartHeuristicAgent())
            {
                if (args[1] == "B")
                {
                    mm = new Minimax(b, Enumerations.Color.BLACK, ha);
                }
                else
                {
                    mm = new Minimax(b, Enumerations.Color.WHITE, ha);
                }
            }else
            {
                return -1;
            }

            Board nb = mm.FindNextMove(nPly);
            ha.Kill();
            //PipeHelper.PushData("HeuristicAgent", "C");
            
            if(nb == null)
            {
                return -1;
            }else
            {
                DumpBoard(nb);
                return 0;
            }
        }

        static Board GetBoardFromFile()
        {
            var lines = File.ReadAllLines(BOARD_FILENAME);
            Board b = new Board(lines[0]);
            return b;
        }

        static void DumpBoard(Board b)
        {
            using (var sw = new StreamWriter(BOARD_FILENAME, false))
            {
                sw.Write(b.ParameterString);
            }

        }
    }
}
