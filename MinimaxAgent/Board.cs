﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinimaxAgent
{
    class Board
    {
        public static char NEUTRAL_CHAR;
        public static char BLACK_CHAR;
        public static char WHITE_CHAR;

        #region Private Variables
        private const int BOARD_HEIGHT = 8;
        private const int BOARD_WIDTH = 8;
        private Piece[,] _board;
        private Dictionary<Color, HashSet<Move>> _valid_moves;
        private Dictionary<Color, int> _number_of_pieces;
        private Move _last_applied_move;
        private int _heuristic_value;
        private int _pieces_flipped_from_last_move;
        private int _turns_passed;
        #endregion

        #region Properties

        public string ParameterString
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                for (int y = 0; y < BOARD_HEIGHT; ++y)
                {
                    for (int x = 0; x < BOARD_WIDTH; ++x)
                    {
                        if (_board[x, y] == null)
                        {
                            sb.Append(NEUTRAL_CHAR);
                        }
                        else
                        {
                            switch (_board[x, y].CurrentColor)
                            {
                                case Color.BLACK:
                                    sb.Append(BLACK_CHAR);
                                    break;
                                case Color.WHITE:
                                    sb.Append(WHITE_CHAR);
                                    break;
                            }
                        }
                    }
                }
                return sb.ToString();
            }
        }

        public int HeuristicValue
        {
            get
            {
                return _heuristic_value;
            }
        }

        public Move LastAppliedMove
        {
            get
            {
                return _last_applied_move;
            }
        }
        #endregion

        #region Constructors
        public Board()
        {
            _heuristic_value = int.MaxValue;
            _board = new Piece[8, 8];
            _board[3, 3] = new Piece(Color.WHITE);
            _board[4, 4] = new Piece(Color.WHITE);
            _board[4, 3] = new Piece(Color.BLACK);
            _board[3, 4] = new Piece(Color.BLACK);
            _valid_moves = new Dictionary<Color, HashSet<Move>>();
            _valid_moves.Add(Color.BLACK, new HashSet<Move>());
            _valid_moves.Add(Color.WHITE, new HashSet<Move>());
            UpdateValidMoves();
            _turns_passed = 0;
        }

        public Board(string initialBoard)
        {
            _heuristic_value = int.MaxValue;
            _board = new Piece[BOARD_WIDTH, BOARD_HEIGHT];
            _number_of_pieces = new Dictionary<Color, int>();
            _number_of_pieces.Add(Color.BLACK, 0);
            _number_of_pieces.Add(Color.WHITE, 0);
            _valid_moves = new Dictionary<Color, HashSet<Move>>();
            _valid_moves.Add(Color.BLACK, new HashSet<Move>());
            _valid_moves.Add(Color.WHITE, new HashSet<Move>());
            int nbPieces = 0;
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (initialBoard[y * BOARD_WIDTH + x] != NEUTRAL_CHAR)
                    {
                        if (initialBoard[y * BOARD_WIDTH + x] == BLACK_CHAR)
                        {
                            _board[x, y] = new Piece(Color.BLACK);
                            ++_number_of_pieces[Color.BLACK];
                        }
                        else
                        {
                            _board[x, y] = new Piece(Color.WHITE);
                            ++_number_of_pieces[Color.WHITE];
                        }
                        ++nbPieces;
                    }
                }
            }
            UpdateValidMoves();
            _turns_passed = nbPieces - 4;
        }
        #endregion

        #region Private Functions
        private void UpdateValidMoves()
        {
            _valid_moves[Color.BLACK].Clear();
            _valid_moves[Color.WHITE].Clear();
            for (int y = 0; y < BOARD_HEIGHT; ++y)
            {
                for (int x = 0; x < BOARD_WIDTH; ++x)
                {
                    if (_board[x, y] == null)
                    {
                        Position p = new Position(x, y);
                        AddAnyValidMove(Color.BLACK, p, Direction.UP);
                        AddAnyValidMove(Color.BLACK, p, Direction.DOWN);
                        AddAnyValidMove(Color.BLACK, p, Direction.RIGHT);
                        AddAnyValidMove(Color.BLACK, p, Direction.LEFT);
                        AddAnyValidMove(Color.BLACK, p, Direction.DIAG_LU);
                        AddAnyValidMove(Color.BLACK, p, Direction.DIAG_RU);
                        AddAnyValidMove(Color.BLACK, p, Direction.DIAG_LD);
                        AddAnyValidMove(Color.BLACK, p, Direction.DIAG_RD);
                        AddAnyValidMove(Color.WHITE, p, Direction.UP);
                        AddAnyValidMove(Color.WHITE, p, Direction.DOWN);
                        AddAnyValidMove(Color.WHITE, p, Direction.RIGHT);
                        AddAnyValidMove(Color.WHITE, p, Direction.LEFT);
                        AddAnyValidMove(Color.WHITE, p, Direction.DIAG_LU);
                        AddAnyValidMove(Color.WHITE, p, Direction.DIAG_RU);
                        AddAnyValidMove(Color.WHITE, p, Direction.DIAG_LD);
                        AddAnyValidMove(Color.WHITE, p, Direction.DIAG_RD);
                    }

                }
            }
        }

        private void AddAnyValidMove(Color c, Position sourcePosition, Direction d)
        {
            Position nextPos = sourcePosition.Move(d);
            bool hasVisitedOnePiece = false;
            while (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor != c)
            {
                hasVisitedOnePiece = true;
                nextPos = nextPos.Move(d);
            }
            if (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor == c)
            {
                if (hasVisitedOnePiece)
                {
                    Move m = new Move(c, sourcePosition.X, sourcePosition.Y);
                    if (!_valid_moves[c].Contains(m))
                    {
                        _valid_moves[c].Add(m);
                    }
                }
            }
        }
        private int FlipPieces(Direction d, Piece sourcePiece, Position sourcePos)
        {
            Position nextPos = sourcePos.Move(d);
            List<Piece> piecesToFlip = new List<Piece>();
            while (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor != sourcePiece.CurrentColor)
            {
                piecesToFlip.Add(_board[nextPos.X, nextPos.Y]);
                nextPos = nextPos.Move(d);
            }
            if (nextPos != null && _board[nextPos.X, nextPos.Y] != null && _board[nextPos.X, nextPos.Y].CurrentColor == sourcePiece.CurrentColor)
            {
                foreach (var ptf in piecesToFlip)
                {
                    ptf.Flip();
                    ++_number_of_pieces[sourcePiece.CurrentColor];
                    --_number_of_pieces[sourcePiece.CurrentColor.Opposite()];
                }
                return piecesToFlip.Count;
            }
            return 0;
        }

        private bool PlayMove(Move m)
        {
            if (_valid_moves[m.MoveColor].Contains(m))
            {
                Piece p = new Piece(m.MoveColor);
                _board[m.MovePosition.X, m.MovePosition.Y] = p;
                ++_number_of_pieces[m.MoveColor];
                Position sp = new Position(m.MovePosition.X, m.MovePosition.Y);
                _pieces_flipped_from_last_move = 0;
                _pieces_flipped_from_last_move += FlipPieces(Direction.DIAG_LD, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.DIAG_RD, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.DIAG_LU, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.DIAG_RU, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.UP, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.DOWN, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.RIGHT, p, sp);
                _pieces_flipped_from_last_move += FlipPieces(Direction.LEFT, p, sp);
                UpdateValidMoves();
                _last_applied_move = m;
                ++_turns_passed;
                return true;
            }
            return false;
        }
        #endregion

        #region Public Functions
        public void DumpValidMoves(Color movesFor)
        {
            Console.WriteLine(String.Format("Valid moves for {0}", movesFor));
            foreach (var m in _valid_moves[movesFor])
            {
                Console.WriteLine(m);
            }
        }

        public bool GetHasValidMoves(Color c)
        {
            return _valid_moves[c].Count > 0;
        }

        public IEnumerable<Board> GetChildrenBoards(Color c)
        {
            foreach (var m in _valid_moves[c])
            {
                Board b = new Board(ParameterString);
                if (b.PlayMove(m))
                {
                    yield return b;
                }
                else
                {
                    continue;
                }
            }
        }
        #endregion

        #region Member Overrides
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = -1; y < BOARD_HEIGHT; ++y)
            {
                for (int x = -1; x < BOARD_WIDTH; ++x)
                {
                    if (y < 0 || x < 0)
                    {
                        if (x < 0)
                        {
                            if (y >= 0)
                                sb.Append(y);
                            else
                                sb.Append(' ');
                        }
                        else
                        {
                            sb.Append(x);
                        }
                    }
                    else
                    {
                        if (_board[x, y] == null)
                        {
                            sb.Append("-");
                        }
                        else
                        {
                            switch (_board[x, y].CurrentColor)
                            {
                                case Color.BLACK:
                                    sb.Append(BLACK_CHAR);
                                    break;
                                case Color.WHITE:
                                    sb.Append(WHITE_CHAR);
                                    break;
                            }
                        }
                    }

                }
                if (y < BOARD_HEIGHT - 1)
                {
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }
        #endregion

    }
}
